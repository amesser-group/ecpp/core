/*
 * Ringbuffer.hpp
 *
 *  Created on: 14.05.2015
 *      Author: andi
 */

#ifndef ECPP_RINGBUFFER_HPP_
#define ECPP_RINGBUFFER_HPP_

#include "ecpp/Datatypes.hpp"

namespace ecpp {
  template<typename TYPE, unsigned long SIZE>
  class Ringbuffer {
  public:
    typedef typename UnsignedIntTypeEstimator<SIZE>::Type IndexType;

  private:
    volatile IndexType write_index_;
    volatile IndexType read_index_;

    TYPE _buf[SIZE];

  public:

    /** STL like size member function  */
    constexpr        IndexType size()      const { return write_index_ - read_index_; }
    /** STL like max size function  */
    static constexpr IndexType max_size()        { return SIZE; }

    /** Returns free space in ring buffer */
    constexpr IndexType space()     const { return max_size() - size(); }

    static constexpr IndexType getSize() {return SIZE;}
    IndexType getCount() const {return write_index_ - read_index_;}

    void reset() {
      write_index_ = 0;
      read_index_ = 0;
    }

    static constexpr IndexType CalcBufferIndex(IndexType idx)
    {
      return idx % getSize();
    }


    constexpr IndexType getInsertPos() const
    {
      return CalcBufferIndex(write_index_);
    }

    TYPE* getInsertElem()
    {
      if(getCount() < getSize())
      {
        return &_buf[getInsertPos()];
      }
      {
        return 0;
      }
    }

    TYPE (& getBuffer()) [SIZE] { return _buf;}

    TYPE pop() {
      const IndexType ReadIndex = read_index_;

      while (static_cast<IndexType>((write_index_ - ReadIndex)) == 0);

      TYPE data = _buf[ReadIndex % getSize()];
      read_index_ = ReadIndex + 1;

      return data;
    }

    TYPE popForced() {
      const IndexType ReadIndex = read_index_;

      TYPE data = _buf[ReadIndex % getSize()];
      read_index_ = ReadIndex + 1;

      return data;
    }

    void push(const TYPE& data) {
      const IndexType WriteIndex = write_index_;

      while (static_cast<IndexType>(WriteIndex - read_index_) == getSize());

      _buf[WriteIndex % getSize()] = data;
      write_index_ = WriteIndex+1;

    }

    void pushForced(const TYPE& data) {
      const IndexType WriteIndex = write_index_;

      _buf[WriteIndex % getSize()] = data;
      write_index_ = WriteIndex+1;

    }

    void pushForced()
    {
      const IndexType WriteIndex = write_index_;
      write_index_ = WriteIndex+1;
    }

    inline TYPE & front() {
      return _buf[read_index_ % getSize()];
    }

    inline const TYPE & front() const {
      return _buf[read_index_ % getSize()];
    }

    inline TYPE & back() {
      return _buf[(write_index_ - 1) % getSize()];
    }

    inline const TYPE & back() const {
      return _buf[(write_index_ - 1) % getSize()];
    }

    void write(IndexType offset, const TYPE &t)
    {
      _buf[CalcBufferIndex(write_index_ + offset)] = t;
    }

    void commit(IndexType count)
    {
      write_index_ += count;
    }

    class Writer;
  };

  template<typename TYPE, unsigned long SIZE>
  class Ringbuffer<TYPE, SIZE>::Writer
  {
  public:
    Writer(Ringbuffer & ring) :
      write_index_(ring.write_index_)
    {
      
    }
  private:
    IndexType & write_index_;
  };
}

#endif /* ECPP_RINGBUFFER_HPP_ */
