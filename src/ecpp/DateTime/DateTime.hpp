/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_DATETIME_DATETIME_HPP_
#define ECPP_DATETIME_DATETIME_HPP_

#include "ecpp/DateTime/Time.hpp"
#include "ecpp/DateTime/Date.hpp"
#include <cstdint>

namespace ecpp::DateTime
{  
  class DateTime
  {
  public:
    class kOneSecondTag {};

    using Month = Date::Month;

    constexpr DateTime(const Date& date, const Time& time) :
      date_(date), time_(time) {}

    constexpr auto valid() const  { return date_.valid() and time_.valid(); }

    constexpr auto & date() const { return date_; }
    constexpr auto & time() const { return time_; }
    
    constexpr auto hour()   const {return time_.hour();}
    constexpr auto minute() const {return time_.minute();}
    constexpr auto second() const {return time_.second();}

    bool operator<  (const DateTime & rhs) const;
    bool operator>= (const DateTime & rhs) const;

    const DateTime & operator +=(const kOneSecondTag&);


    static constexpr kOneSecondTag kOneSecond {};
    static const     DateTime kInvalid;
    static const     DateTime kZero;
  protected:
    Date date_;
    Time time_;  
  };

  constexpr const DateTime DateTime::kInvalid {Date::kInvalid, Time::kInvalid};
  constexpr const DateTime DateTime::kZero    {Date::kZero,    Time::kZero};
}

#endif