/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_DATETIME_TIME_HPP_
#define ECPP_DATETIME_TIME_HPP_

#include <cstdint>

namespace ecpp::DateTime
{  
  /** A simple container to hold a time and do simple oprations on it */
  class Time
  {
  public:
    class kOneSecondTag {};

    static constexpr kOneSecondTag kOneSecond {};
    static const Time              kInvalid;
    static const Time              kZero;

    constexpr Time(int_least8_t hour, int_least8_t minute, int_least8_t second) :
      second_(second), minute_(minute), hour_(hour) {}

    constexpr bool valid()   const { return (second_ >= 0) && (minute_ >= 0) && (hour_ >= 0); }

    constexpr auto hour()   const { return hour_;   }
    constexpr auto minute() const { return minute_; }
    constexpr auto second() const { return second_; }

    constexpr int32_t count() const
    {
      return (static_cast<int32_t>(hour_) * 60 + static_cast<int32_t>(minute_)) * 60 + second_;
    }

    constexpr bool operator==(const Time & rhs) const
    {
      return count() == rhs.count();
    }

    constexpr bool operator<(const Time & rhs) const
    {
      return count() < rhs.count();
    }

    constexpr bool operator>=(const Time & rhs) const
    {
      return count() >= rhs.count();
    }

    constexpr Time operator+ (const kOneSecondTag&) const
    {
      if(!valid())
        return kInvalid;
      if(second() < 59)
        return { hour(), minute(), static_cast<int_least8_t>(second() + 1)};
      else if (minute() < 59)
        return { hour(), static_cast<int_least8_t>(minute() + 1), 0};
      else if (hour() < 23)
        return { static_cast<int_least8_t>(hour() + 1), 0, 0};
      else
        return kZero;
    }

    static constexpr int32_t kMaxSecondOfDay = 24*60*60 - 1;
  protected:
    int_least8_t second_;
    int_least8_t minute_;
    int_least8_t hour_;
  };

  constexpr const Time Time::kInvalid {-1,-1,-1};
  constexpr const Time Time::kZero    {0,0,0};

}

#endif