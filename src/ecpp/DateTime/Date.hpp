/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_DATETIME_DATE_HPP_
#define ECPP_DATETIME_DATE_HPP_

#include <cstdint>

namespace ecpp::DateTime
{  
  /** A simple container to hold a time */
  class Date
  {
  public:
    class kOneDayTag {};

    static constexpr kOneDayTag kOneDay {};
    
    enum Month : int_least8_t {
      Invalid   = -1,
      January   = 1,
      February  = 2,
      March     = 3,
      April     = 4,
      May       = 5,
      June      = 6,
      July      = 7,
      August    = 8,
      September = 9,
      October   = 10,
      November  = 11,
      December  = 12,
    };

    constexpr Date(int_least8_t century, int_least8_t year, Month month, int_least8_t day) :
      century_(century), year_(year), month_(month), day_(day) {}

    int_least8_t GetDaysOfMonth() const;

    constexpr bool valid()   const { return (month_ >= 1) && (month_ <= 12) && (day_ >= 1) && (day_ <= GetDaysOfMonth()); }

    constexpr bool IsLeap() const
    {
      return ((year_ == 0) ? century_ : year_) % 4 == 0;
    }

    constexpr int32_t count() const
    {
      /* compose a monotonic number in a simple fashion */
      return ((static_cast<int32_t>(century_) * 256 + year_) *256 + month_) * 256 + day_;
    }

    constexpr bool operator<(const Date & rhs) const
    {
      return count() < rhs.count();
    }

    constexpr bool operator>(const Date & rhs) const
    {
      return count() > rhs.count();
    }

    constexpr bool operator>=(const Date & rhs) const
    {
      return count() >= rhs.count();
    }

    Date operator + (const kOneDayTag& ) const;

    static const Date kInvalid;
    static const Date kZero;
  protected: 

    int_least8_t century_;
    int_least8_t year_;
    Month        month_;
    int_least8_t day_;
  };

  constexpr const Date Date::kInvalid {-1,-1,Date::Month::Invalid,-1};
  constexpr const Date Date::kZero    {20, 0,Date::Month::January, 1};
}

#endif