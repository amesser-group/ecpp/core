/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#include "ecpp/DateTime/Date.hpp"

using namespace ::ecpp::DateTime;

int_least8_t
Date::GetDaysOfMonth() const
{
  if(month_ == February)
    return IsLeap() ? 29 : 28;
  else if (month_ < August)
    return 30 + (month_ % 2);
  else
    return 31 - (month_ % 2);
}

Date 
Date::operator+ (const Date::kOneDayTag &) const
{
  if(!valid())
    return kInvalid;
  else if(day_ < GetDaysOfMonth())
    return { century_, year_, month_, static_cast<int_least8_t>(day_+1)};
  else if(month_ < 12)
    return {century_, year_, static_cast<Month>(month_ + 1), 1};
  else if(year_ < 99)
    return { century_, static_cast<int_least8_t>(year_ + 1), January, 1};
  else
    return { static_cast<int_least8_t>(century_ + 1), 0, January, 1};
}