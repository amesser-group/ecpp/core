/*
 *  Copyright 2023 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */

#include <cstddef>
#include <cstdint>
#include <cassert>

extern "C" void *  _sbrk (ptrdiff_t __incr);

namespace ecpp::System
{
  /* newlib needs a lot of memory for heap processing */
  static constexpr size_t kHeapSize = 4096;

  static struct 
  {
    ptrdiff_t               sbrk_offset = 0;
    uint8_t                 buffer[kHeapSize] __attribute__((aligned(16)));
  } heap;
};

void *  _sbrk (ptrdiff_t __incr)
{
  auto & heap = ecpp::System::heap;
  auto current_offset = heap.sbrk_offset;
  

  assert( (current_offset + __incr) <= sizeof(heap.buffer));

  if( (current_offset + __incr) > sizeof(heap.buffer))
    return nullptr;

  heap.sbrk_offset = current_offset + __incr;

  return &(heap.buffer[current_offset]);
}