/*
 *  Copyright 2023 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_SYSTEM_TICKTIMER_HPP_
#define ECPP_SYSTEM_TICKTIMER_HPP_

#include "ecpp/System/Tick.hpp"
#include "ecpp/Units/Time.hpp"

namespace ecpp::System
{ 
  /** A timer implementation storing the start time
   */
  class TickTimer
  {
  public:
    constexpr TickTimer() {}

    void Start() { ts_started_ = GetTick(); }

    template<typename ValueType, typename Scale>
    void Continue(ecpp::Units::TimeSpan<ValueType, Scale> cont) { ts_started_ += cont; }

    /** The system tick value the timer expires. Can be */
    constexpr auto WhenStarted() const { return ts_started_; }

    /** check if the given amount of time has passed since last start */
    template<typename ValueType, typename Scale>
    bool IsExpired(ecpp::Units::TimeSpan<ValueType, Scale> timeout) const
    { 
      return (GetTick() - ts_started_) >= timeout; 
    }

  protected:
    Tick ts_started_;
  };

  /** A timer implementation storing the timeout deadline */
  class DeadlineTimer
  {
  public:
    constexpr auto deadline() const { return ts_deadline_; }
    
    template<typename ValueType, typename Scale>
    void Start(ecpp::Units::TimeSpan<ValueType, Scale> timeout)
    {
      ts_deadline_ = GetTick() + timeout; 
    }

    template<typename ValueType, typename Scale>
    void Continue(ecpp::Units::TimeSpan<ValueType, Scale> timeout)
    {
      ts_deadline_ += timeout; 
    }

    bool IsExpired(Tick ref) const 
    { 
      return not ref.IsBefore(ts_deadline_); 
    }

    bool IsExpired() const 
    {
      return IsExpired(GetTick());
    }

  protected:
    Tick ts_deadline_;
  };
}

#endif