/*
 *  Copyright 2015-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_STRING_HPP_
#define ECPP_STRING_HPP_

#include "ecpp/Datatypes.hpp"
#include "ecpp/Iterator.hpp"

#include <type_traits>
#include <utility>
#include <cstring>
#include <iterator>
#include <algorithm>

namespace ecpp
{
  template<typename _Encoding>
  class String
  {
  public:
    using Encoding      = _Encoding;
    using BufferElement = typename Encoding::BufferElement;
    using Codepoint     = typename Encoding::Codepoint;
  protected:
    class ConstStringIterator
    {
    public:
      using iterator_category = std::input_iterator_tag;
      using value_type      = const Codepoint;
      using difference_type = intptr_t;
      using pointer         = const Codepoint *;
      using reference       = const Codepoint&;
    };

    class StringIterator
    {
    public:
      using iterator_category = std::output_iterator_tag;
      using value_type      = Codepoint;
      using difference_type = intptr_t;
      using pointer         = Codepoint*;
      using reference       = Codepoint&;
    };
  };

  template<typename _Encoding>
  class StringView;

  template<typename _Encoding>
  class ZeroTerminatedString : public String<_Encoding>
  {
  public:
    using Encoding      = _Encoding;
    using BufferElement = typename Encoding::BufferElement;
    using Codepoint     = typename Encoding::Codepoint;

    class ConstIterator;
  };

  template<typename _Encoding>
  class ZeroTerminatedString<_Encoding>::ConstIterator : protected _Encoding::Decoder, public String<_Encoding>::ConstStringIterator
  {
  public:
    constexpr ConstIterator(const BufferElement* ptr) : _Encoding::Decoder{ptr} {}

    constexpr Codepoint operator*() const 
    { 
      return _Encoding::Decoder::DecodeChar(nullptr);
    }

    ConstIterator operator++ (int)
    {
      auto ret = ConstIterator(*this);
      _Encoding::Decoder::NextChar(nullptr);
      return ret;
    }

    ConstIterator & operator++ ()
    {
      _Encoding::Decoder::NextChar(nullptr);
      return (*this);
    }

    constexpr bool operator== (const ::ecpp::EndIterationTag &) const
    {
      return _Encoding::Decoder::DecodeChar(nullptr) == Codepoint::kStringEnd;
    }

    constexpr bool operator!= (const ::ecpp::EndIterationTag &) const
    {
      return _Encoding::Decoder::DecodeChar(nullptr) != Codepoint::kStringEnd;
    }

    constexpr bool operator< (const ::ecpp::EndIterationTag &) const
    {
      return _Encoding::Decoder::DecodeChar(nullptr) != Codepoint::kStringEnd;
    }

    /* some extras to make ::std iterator stuff work */
    constexpr bool operator== (const ConstIterator & rhs) const
    {
      if (rhs.buf_ == nullptr)
        return operator==(::ecpp::EndIterationTag() );
      else
        return(rhs.buf_ == this->buf_);
    }

    constexpr bool operator!= (const ConstIterator & rhs) const
    {
      if (rhs.buf_ == nullptr)
        return operator!=(::ecpp::EndIterationTag() );
      else
        return(rhs.buf_ != this->buf_);
    }

    constexpr const BufferElement* ToPointer() const { return this->buf_;}
  };



  template<typename _Encoding>
  class StringPointer : public ZeroTerminatedString<_Encoding>
  {
  public:
    using typename ZeroTerminatedString<_Encoding>::ConstIterator;

    constexpr StringPointer(const char* ptr) : ptr_{ptr} {}

    constexpr auto begin() const { return ConstIterator(ptr_); }
    constexpr auto end()   const { return EndIterationTag(); }

    constexpr auto CountCharacters() const { return ::std::distance(begin(), ConstIterator(nullptr)); }
  protected:
    const char* ptr_;
  };

  template<typename _Encoding>
  class StringSpan : public String<_Encoding>
  {
  public:
    class ConstIterator;
    class Iterator;

  protected:
    using Encoding      = _Encoding;
    using BufferElement = typename Encoding::BufferElement;
    using Codepoint     = typename Encoding::Codepoint;
  };

  template<typename _Encoding>
  class StringSpan<_Encoding>::ConstIterator : protected _Encoding::Decoder, public String<_Encoding>::ConstStringIterator
  {
  public:
    constexpr ConstIterator(const BufferElement* ptr, const BufferElement* end) : _Encoding::Decoder{ptr}, end_{end} {}

    constexpr Codepoint operator*() const 
    { 
      return _Encoding::Decoder::DecodeChar(end_);
    }

    ConstIterator & operator++ ()
    {
      _Encoding::Decoder::NextChar(end_);
      return (*this);
    }

    ConstIterator operator++ (int)
    {
      auto ret = ConstIterator(*this);
      _Encoding::Decoder::NextChar(end_);
      return ret;
    }

    constexpr bool operator== (const ::ecpp::EndIterationTag &) const
    {
      return _Encoding::Decoder::DecodeChar(end_) == Codepoint::kStringEnd;
    }

    constexpr bool operator!= (const ::ecpp::EndIterationTag &) const
    {
      return _Encoding::Decoder::DecodeChar(end_) != Codepoint::kStringEnd;
    }

    constexpr bool operator< (const ::ecpp::EndIterationTag &) const
    {
      return _Encoding::Decoder::DecodeChar(end_) != Codepoint::kStringEnd;
    }

    constexpr const BufferElement* ToPointer() const { return this->buf_;}

    /* some extras to make ::std iterator stuff work */
    constexpr bool operator== (const ConstIterator & rhs) const
    {
      if (rhs.buf_ == nullptr)
        return operator==(::ecpp::EndIterationTag() );
      else
        return(rhs.buf_ == this->buf_);
    }

    constexpr bool operator!= (const ConstIterator & rhs) const
    {
      if (rhs.buf_ == nullptr)
        return operator!=(::ecpp::EndIterationTag() );
      else
        return(rhs.buf_ != this->buf_);
    }
  protected:
    const BufferElement *end_;

    friend StringView<_Encoding>;
  };

  template<typename _Encoding>
  class StringSpan<_Encoding>::Iterator : protected _Encoding::Encoder, public String<_Encoding>::StringIterator
  {
  public:
    constexpr Iterator(BufferElement* ptr, const BufferElement* end) : _Encoding::Encoder{ptr}, end_{end} {}

    constexpr Iterator& operator*() 
    { 
      return *this;
    }

    Iterator & operator++ ()
    {
      _Encoding::Encoder::NextChar(end_);
      return (*this);
    }

    Iterator & operator+ (int count)
    {
      for(;count>0;--count)
        _Encoding::Encoder::NextChar(end_);
      return (*this);
    }

    void operator=(Codepoint cp)
    {
      if(_Encoding::Encoder::EncodeChar(cp, end_) == end_)
        this->buf_ = end_;
    }

    constexpr bool operator== (const ::ecpp::EndIterationTag &) const
    {
      return this->buf_ >= end_;
    }

    constexpr bool operator!= (const ::ecpp::EndIterationTag &) const
    {
      return this->buf_ < end_;
    }

    constexpr bool operator< (const ::ecpp::EndIterationTag &) const
    {
      return this->buf_ < end_;
    }

    constexpr bool operator< (const Iterator &rhs) const
    {
      return this->buf_ < rhs.buf_;
    }

  protected:
    const BufferElement * const end_;
  };


  template<typename _Encoding>
  class StringView : protected StringSpan<_Encoding>
  {
  public:
    using typename StringSpan<_Encoding>::ConstIterator;
    using typename StringSpan<_Encoding>::BufferElement;

    template<typename BaseString>
    static StringView Trim(const BaseString &base);

    constexpr ConstIterator begin()  const  { return ConstIterator(buf_, end_); }
    constexpr EndIterationTag end()   const { return EndIterationTag(); }

    constexpr auto CountCharacters() const { return ::std::distance(begin(), ConstIterator(nullptr, nullptr)); }

    constexpr StringView(ConstIterator init) : buf_{ init.buf_}, end_{ init.end_} {}
  protected:
    template<typename Start, typename End>
    constexpr StringView(const Start & start, const End & end) : buf_{start.ToPointer()}, end_{end.ToPointer()} {}

    const BufferElement *buf_;
    const BufferElement *end_;
  };

  template<typename _Encoding>
  template<typename BaseString>
  StringView<_Encoding> StringView<_Encoding>::Trim(const BaseString &base)
  {
    auto start = base.begin();
    for(; start < base.end(); ++start)
      if ((*start).IsVisible())
        break;

    auto end = start;

    for(auto it = end; it < base.end(); ++it)
      if ((*it).IsVisible())
        end = it;

    ++end;

    return {start, end};
  }

  template<typename OutputEncoding, typename InputString>
  class EncodingConverterIterator
  {
  public:
    using Codepoint = typename OutputEncoding::Codepoint;

    constexpr EncodingConverterIterator(const InputString &str) : str_{str}, in_{str.begin()} {}

    constexpr Codepoint operator*() const 
    { 
      return static_cast<Codepoint>(*in_);
    }

    EncodingConverterIterator & operator++ ()
    {
      ++in_;
      return (*this);
    }

    template<typename RHS>
    constexpr bool operator== (RHS && rhs) const
    {
      return in_ == ::std::forward<RHS>(rhs);
    }

    template<typename RHS>
    constexpr bool operator!= (RHS && rhs) const
    {
      return in_ != ::std::forward<RHS>(rhs);
    }

    template<typename RHS>
    constexpr bool operator< (RHS && rhs) const
    {
      return in_ == ::std::forward<RHS>(rhs);
    }

  protected:
    const InputString                 & str_;
    typename InputString::ConstIterator in_;
  };


  class String_
  {
  public:
    static bool convertToHex(char *hex, uint8_t length, uint16_t value);
    static bool convertToDecimal(char *decimal, uint8_t length, uint16_t value);

    static size_t getLTrimLen(const char* text, size_t len);

    template<size_t ARRSIZE>
    static size_t getLTrimLen(const char (& text)[ARRSIZE]) { return getLTrimLen(text, ARRSIZE);}

    static size_t getRTrimStringLen(const char* text, size_t len);

    template<size_t ARRSIZE>
    static size_t getRTrimStringLen(const char (& text)[ARRSIZE]) { return getRTrimStringLen(text, ARRSIZE);}


    static void
    formatUnsigned(char *buffer, uint8_t digits, uint16_t value, char fill =' ')
    {
     uint_fast8_t i;
     bool overflow;

     overflow = convertToDecimal(buffer, digits, value);

     if(overflow)
     {
       fill = '#';
     }

     for(i = 0; i < digits; ++i)
     {
       if (overflow)
       {
         buffer[i] = fill;
       }
       else if ((i + 1) >= digits)
       {
         break;
       }
       else if (buffer[i] != '0')
       {
         break;
       }
       else
       {
         buffer[i] = fill;
       }
     }
    }

    /** insert a decimal point into the given formated number */
    static void
    formatFrac(char *buffer, uint8_t digits, uint8_t prec)
    {
      uint8_t i;

      for (i = digits; (i > 1); --i)
      {
        if (i < (digits - prec - 1))
          break;

        if (buffer[i-1] == '-')
        {
          buffer[i-2] = '-';
          buffer[i-1] = '0';
        }
        else if (buffer[i-1] == ' ')
        {
          buffer[i-1] = '0';
        }
      }

      while(digits >= 2)
      {
        digits = digits - 1;

        if (prec > 0)
        {
          buffer[digits] = buffer[digits-1];
          prec--;
        }
        else
        {
          buffer[digits] = '.';
          break;
        }
      }
    }

    static void
    formatSigned(char *buffer, int_fast8_t digits, int16_t value, char fill =' ', char sign = '\x00')
    {
      int_fast8_t i;
      bool overflow;

      if(value >= 0)
      {
        overflow = convertToDecimal(buffer, digits, value);
        if (sign == 0)
        {
          sign = fill;
        }
      }
      else
      {
        overflow = convertToDecimal(buffer, digits, -value);
        sign = '-';
        overflow = overflow | (buffer[0] != '0');
      }

      if (overflow)
      {
        memset(buffer, '#', digits);
      }
      else if (buffer[0] == '0' && (digits > 1))
      {
        for(i = 1; i < (digits-1); ++i)
        {
          buffer[i-1] = fill;

          if(buffer[i] != '0')
            break;
        }
        buffer[i-1] = sign;
      }
    }
  };
};




#endif /* ECPP_STRING_HPP_ */

