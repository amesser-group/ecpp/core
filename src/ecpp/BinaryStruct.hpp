/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_BINARYSTRUCT_HPP_
#define ECPP_BINARYSTRUCT_HPP_

#include <cstdint>
#include <cstring>
#include <array>

namespace ecpp
{
  class BinaryData
  {
  public:
    constexpr      BinaryData(void* buffer) : buffer_(static_cast<::std::uint8_t*>(buffer)) {}
    constexpr bool IsValid()       const { return buffer_ != nullptr; }

    void    SetField8(unsigned int idx, ::std::uint8_t value)
    {
      buffer_[idx] = value;
    }

    ::std::uint8_t GetField8(unsigned int idx) const
    {
      return buffer_[idx];
    }

    ::std::int8_t  GetSigned8(unsigned int idx) const
    {
      return static_cast<int8_t>(GetField8(idx));
    }

    void SetField16(unsigned int idx, ::std::uint16_t value)
    {
      buffer_[idx]   = value & 0xFF;
      buffer_[idx+1] = (value >> 8) & 0xFF;
    }

    ::std::uint16_t GetField16(unsigned int idx) const
    {
      return  buffer_[idx] |
             (buffer_[idx+1] << 8);
    }

    ::std::int16_t  GetSigned16(unsigned int idx) const
    {
      return static_cast<::std::int16_t>(GetField16(idx));
    }

    void SetField32(unsigned int idx, ::std::uint32_t value)
    {
      buffer_[idx]   = value & 0xFF;
      buffer_[idx+1] = (value >> 8) & 0xFF;
      buffer_[idx+2] = (value >> 16) & 0xFF;
      buffer_[idx+3] = (value >> 24) & 0xFF;
    }

    ::std::uint32_t GetField32(unsigned int idx) const
    {
      return       buffer_[idx] |
                  (buffer_[idx+1] <<  8) |
        ((uint32_t)buffer_[idx+2] << 16) |
        ((uint32_t)buffer_[idx+3] << 24);
    }

    template<typename T>
    T & GetBlock(unsigned int idx)
    {
      return *reinterpret_cast<T*>(&(buffer_[idx]));
    }

    template<typename T>
    T & SetBlock(unsigned int idx, const T& block)
    {
      return *reinterpret_cast<T*>(SetBlockInternal(idx, &block, sizeof(block)));
    }

  protected:
    uint8_t* SetBlockInternal(unsigned int idx, const void* data, size_t data_len)
    {
      memcpy(&(buffer_[idx]), reinterpret_cast<const uint8_t*>(data), data_len);
      return &(buffer_[idx]);
    }

    ::std::uint8_t *buffer_;
  };

  class BinaryStruct : public BinaryData
  {
  public:
    constexpr BinaryStruct(void* buffer, size_t buffer_len) : BinaryData(buffer), buffer_len_ (buffer_len) {}

    template<typename Buffer>
    constexpr BinaryStruct(Buffer &buffer) : BinaryData(&buffer), buffer_len_ (sizeof(Buffer)) {}

    constexpr size_t GetBufferSize() const { return buffer_len_; }

    /** Pad a given length structure with data till end */
    size_t Pad(unsigned int idx, const void* data, size_t data_len)
    {
      if (idx > buffer_len_)
        return 0;

      if ((idx + data_len) > buffer_len_)
        data_len = buffer_len_ - idx;

      BinaryData::SetBlockInternal(idx, data, data_len);

      return data_len;
    }
  protected:
    const size_t buffer_len_;
  };


  template<class Buffer>
  class SerializingBuffer : protected Buffer
  {
  public:
    constexpr SerializingBuffer(const Buffer & buf, size_t offset) : Buffer(buf), offset_(offset) {}

    constexpr bool   OverflowOccured() const { return offset_ > Buffer::GetBufferSize(); }

    void Skip(size_t bytes)
    {
      if (GetRemainSpace() >= bytes)
        offset_ += bytes;
      else
        offset_ = Buffer::GetBufferSize() + 1;
    }

    SerializingBuffer & operator << (char val)
    {
      if (GetRemainSpace() >= 1)
      {
        Buffer::SetField8(offset_, val);
        offset_ += 1;
      }
      else
      {
        offset_ = Buffer::GetBufferSize() + 1;
      }

      return *this;
    }

    SerializingBuffer & operator << (int8_t val)
    {
      if (GetRemainSpace() >= 1)
      {
        Buffer::SetField8(offset_, val);
        offset_ += 1;
      }
      else
      {
        offset_ = Buffer::GetBufferSize() + 1;
      }

      return *this;
    }

    SerializingBuffer & operator << (uint8_t val)
    {
      if (GetRemainSpace() >= 1)
      {
        Buffer::SetField8(offset_, val);
        offset_ += 1;
      }
      else
      {
        offset_ = Buffer::GetBufferSize() + 1;
      }

      return *this;
    }

    SerializingBuffer & operator << (uint16_t val)
    {
      if (GetRemainSpace() >= 2)
      {
        Buffer::SetField16(offset_, val);
        offset_ += 2;
      }
      else
      {
        offset_ = Buffer::GetBufferSize() + 1;
      }

      return *this;
    }

    SerializingBuffer & operator << (uint32_t val)
    {
      if (GetRemainSpace() >= 2)
      {
        Buffer::SetField32(offset_, val);
        offset_ += 4;
      }
      else
      {
        offset_ = Buffer::GetBufferSize() + 1;
      }

      return *this;
    }

    template<typename Type, size_t Count>
    SerializingBuffer & operator << (const ::std::array<Type, Count> &arr)
    {
      for(auto e : arr)
        operator<<(e);

      return *this;
    }

  protected:
    constexpr size_t GetRemainSpace()  const { return (Buffer::IsValid() && (offset_ <= Buffer::GetBufferSize()) ) ? (Buffer::GetBufferSize() - offset_) : 0; }

    size_t offset_ {0};
  };

  template<typename Itr, typename ItrEnd>
  class Serializer
  {
  public:
    constexpr Serializer(Itr it, ItrEnd it_end) : it_(it), end_(it_end) {};

    constexpr bool overflowed() const { return not(it_ < end_); }
    constexpr bool success()    const { return not overflowed(); }
    
    void uint8(uint8_t val, uint8_t default_value = 0) 
    { 
      if(it_ < end_) 
        *(it_++) = val;
    };

    void int8 (int8_t val,  int8_t default_value = 0) 
    { 
      uint8(static_cast<uint8_t>(val));
    };

    void uint16(uint16_t val, uint16_t default_value = 0) 
    { 
      if((it_ + 1) < end_)
      {
        *(it_++) = (val) & 0xFF;
        *(it_++) = (val >> 8) & 0xFF;
      }
      else
      {
        it_ = end_;
      }
    };

    void int16 (int16_t val,  int16_t default_value = 0) 
    { 
      uint16(static_cast<uint16_t>(val));
    };

    void uint32(uint32_t val, uint32_t default_value = 0) 
    { 
      if((it_ + 3) < end_)
      {
        *(it_++) = (val)       & 0xFF;
        *(it_++) = (val >> 8)  & 0xFF;
        *(it_++) = (val >> 16) & 0xFF;
        *(it_++) = (val >> 24) & 0xFF;
      }
      else
      {
        it_ = end_;
      }
    };

    void int32 (int32_t val,  int32_t default_value = 0) 
    { 
      uint32(static_cast<uint32_t>(val));
    };


  protected:
    Itr     it_;
    ItrEnd end_;
  };


  template<typename Itr, typename ItrEnd>
  class Deserializer
  {
  public:
    constexpr Deserializer(Itr it, ItrEnd it_end) : it_(it), end_(it_end) {};

    constexpr bool underflowed() const { return not(it_ < end_); }

    constexpr bool success()    const {return not underflowed(); }
    
    void uint8(uint8_t& val, uint8_t default_value = 0) 
    { 
      if(it_ < end_) 
        val = *(it_++); 
      else 
        val = default_value;
    };

    void int8 (int8_t& val, int8_t default_value = 0) 
    { 
      if(it_ < end_) 
        val = static_cast<int8_t>(*(it_++)); 
      else 
        val = default_value;
    };

    void uint16(uint16_t& val, uint16_t default_value = 0) 
    { 
      if((it_ + 1) < end_) 
      {
        uint16_t tmp;

        tmp = (*(it_++)); 
        tmp |= (*(it_++) << 8);

        val = tmp;
      }
      else 
      {
        it_ = end_;
        val = default_value;
      }
    };

    void int16(int16_t& val, int16_t default_value = 0) 
    { 
      if((it_ + 1) < end_) 
      {
        uint16_t tmp;

        tmp  = (*(it_++)); 
        tmp |= (*(it_++) << 8);

        val = static_cast<int16_t>(tmp);
      }
      else 
      {
        it_ = end_;
        val = default_value;
      }
    };

    void uint32(uint32_t& val, uint32_t default_value = 0) 
    { 
      if((it_ + 1) < end_) 
      {
        uint32_t tmp;

        tmp  = (*(it_++)); 
        tmp |= (*(it_++) << 8);
        tmp |= (*(it_++) << 16);
        tmp |= (*(it_++) << 24);

        val = tmp;
      }
      else 
      {
        it_ = end_;
        val = default_value;
      }
    };

    void int32(int32_t& val, int32_t default_value = 0) 
    { 
      if((it_ + 1) < end_) 
      {
        uint32_t tmp;

        tmp  = (*(it_++)); 
        tmp |= (*(it_++) << 8);
        tmp |= (*(it_++) << 16);
        tmp |= (*(it_++) << 24);

        val = static_cast<int32_t>(tmp);
      }
      else 
      {
        it_ = end_;
        val = default_value;
      }
    };


  protected:
    Itr     it_;
    ItrEnd end_;
  };

  template<class Buffer>
  class DeserializingBuffer : protected Buffer
  {
  public:
    constexpr DeserializingBuffer(const Buffer & buf, size_t offset) : Buffer(buf), offset_(offset) {}

    constexpr bool UnderflowOccured() const { return offset_ > Buffer::GetBufferSize(); }

    void Skip(size_t bytes)
    {
      if (GetRemainData() >= bytes)
        offset_ += bytes;
      else
        offset_ = Buffer::GetBufferSize() + 1;
    }

    DeserializingBuffer & operator >> (char  &val)
    {
      if(GetRemainData() >= 1)
      {
        uint8_t tmp = Buffer::GetField8(offset_);
        val = tmp;
        offset_ += 1;
      }
      else
      {
        offset_  = Buffer::GetBufferSize() + 1;
      }
      return *this;
    }

    DeserializingBuffer & operator >> (int8_t  &val)
    {
      if(GetRemainData() >= 1)
      {
        val = Buffer::GetField8(offset_);
        offset_ += 1;
      }
      else
      {
        offset_  = Buffer::GetBufferSize() + 1;
      }
      return *this;
    }

    DeserializingBuffer & operator >> (uint8_t  &val)
    {
      if(GetRemainData() >= 1)
      {
        val = Buffer::GetField8(offset_);
        offset_ += 1;
      }
      else
      {
        offset_  = Buffer::GetBufferSize() + 1;
      }
      return *this;
    }

    DeserializingBuffer & operator >> (uint16_t &val)
    {
      if(GetRemainData() >= 2)
      {
        val = Buffer::GetField16(offset_);
        offset_ += 2;
      }
      else
      {
        offset_  = Buffer::GetBufferSize() + 1;
      }
      return *this;
    }

    DeserializingBuffer & operator >> (uint32_t &val)
    {
      if(GetRemainData() >= 4)
      {
        val = Buffer::GetField32(offset_);
        offset_ += 4;
      }
      else
      {
        offset_  = Buffer::GetBufferSize() + 1;
      }
      return *this;
    }

    template<typename Type, size_t Count>
    DeserializingBuffer & operator >> (::std::array<Type, Count> &arr)
    {
      for(auto & e : arr)
        operator>>(e);

      return *this;
    }

  protected:
    constexpr size_t GetRemainData()   const { return Buffer::IsValid() ? (Buffer::GetBufferSize() - offset_) : 0; }

    size_t offset_ {0};
  };
}
#endif