/*
 *  Copyright 2015-2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_STRINGENCODINGS_UNICODE_H_
#define ECPP_STRINGENCODINGS_UNICODE_H_

#include "ecpp/String.hpp"
#include "ecpp/StringEncoding.hpp"

namespace ecpp::StringEncodings
{
  class Utf8;

  class Unicode : public ::ecpp::StringEncoding
  {
  public:
    class Codepoint;
  };

  class Utf8 : public Unicode
  {
  public:
    using Unicode::Codepoint;
    using BufferElement = char;

    class Decoder;
    class Encoder;
  };

  class Unicode::Codepoint : public ecpp::StringEncoding::Codepoint
  {
  public:
    constexpr Codepoint()             : val_(0)   {}
    constexpr Codepoint(char32_t val) : val_(val) {}

    bool IsVisible() const;
    
    constexpr char32_t GetRaw() const { return val_;}

    constexpr bool operator ==(const Codepoint &rhs) { return rhs.val_ == val_; }
    constexpr bool operator !=(const Codepoint &rhs) { return rhs.val_ != val_; }

    enum : char32_t
    {
      kStringEnd     = 0,
      kMappingFailed = 0xFFFD,
    };
  protected:
    char32_t  val_;

    friend Utf8;
  };

  class Utf8::Decoder
  {
  public:
    constexpr  Decoder(const BufferElement* buf) : buf_{buf} {};

    const      BufferElement* NextChar (const BufferElement* end);
    Codepoint  DecodeChar              (const BufferElement* end) const;
  protected:
    const      BufferElement* Advance  (const BufferElement* end);

    const BufferElement *buf_;
  };

  class Utf8::Encoder : protected Utf8::Decoder
  {
  public:
    constexpr       Encoder(BufferElement* buf) : Decoder{buf} {};

    const           BufferElement* NextChar (const BufferElement* end);
    BufferElement*  EncodeChar (Codepoint cp, const BufferElement* end);
  };
}

#endif
