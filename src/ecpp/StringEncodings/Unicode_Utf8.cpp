/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#include "ecpp/StringEncodings/Unicode.hpp"
#include "ecpp/String.hpp"

using namespace ::ecpp::StringEncodings;

const Utf8::BufferElement*
Utf8::Decoder::Advance(const Utf8::BufferElement* end)
{
  auto tag = *reinterpret_cast<const uint8_t*>(buf_);

  buf_++;

  if (tag >= 128)
  {
    while((tag & 0x40) && (*buf_ != 0) && (buf_ < end))
    {
      tag <<= 1;
      buf_++;
    }
  }

  return buf_;
}


const Utf8::BufferElement*
Utf8::Decoder::NextChar(const Utf8::BufferElement* end)
{
  if(end == nullptr)
    end = buf_ + 4;
  else if(buf_ >= end)
    return end;

  auto tag = *reinterpret_cast<const uint8_t*>(buf_);

  if (tag != 0)
    return Advance(end);

  return buf_;
}

Utf8::Codepoint
Utf8::Decoder::DecodeChar (const BufferElement* end) const
{
  const uint8_t *ptr = reinterpret_cast<const uint8_t*>(buf_);

  if(end == nullptr)
    end = buf_ + 4;

  if(buf_ >= end)
    return Codepoint::kStringEnd;

  if (*ptr < 128)
    return *ptr;

  if (0x80 == (*ptr & 0xC0))
    return *ptr & 0x3F;

  if ( (0xC0 == (*ptr & 0xE0)) && ((buf_ + 2) <= end))
  {
    if (*(ptr+1) == 0)
      return Codepoint::kStringEnd;

    return (*ptr & 0x1F) << 6 | (*(ptr+1) & 0x3F);
  }

  return Codepoint::kStringEnd;
}

const Utf8::BufferElement*
Utf8::Encoder::NextChar(const Utf8::BufferElement* end)
{
  if(end != nullptr)
    return Advance(end);
  else
    return buf_;
}


Utf8::BufferElement*
Utf8::Encoder::EncodeChar (Codepoint cp, const Utf8::BufferElement* end)
{
  auto *ptr = reinterpret_cast<uint8_t*>(const_cast<BufferElement*>(buf_));
  auto *ptr_end = reinterpret_cast<uint8_t*>(const_cast<BufferElement*>(end));

  if(end == nullptr)
    end = buf_ + 4;

  if(buf_ >= end)
    return reinterpret_cast<BufferElement*>(ptr_end);

  if (cp.val_ < 128)
  {
    *(ptr++) = cp.val_;
  }
  else if (cp.val_ > 0x7FF)
  {
    *(ptr++) = 0x1A;
  }
  else if ((cp.val_ <= 0x7FF) && ((ptr + 2) <= ptr_end))
  {
    *(ptr++) = 0xC0 | ((cp.val_ >> 6) & 0x1F);
    *(ptr++) = 0x80 | ((cp.val_ >> 6) & 0x3F);
  }
  else
  { /* no more space in string */
    *(ptr++) = 0;
    return reinterpret_cast<BufferElement*>(ptr_end);
  }

  if(ptr < ptr_end)
    *ptr = 0;

  return reinterpret_cast<BufferElement*>(ptr);
}


