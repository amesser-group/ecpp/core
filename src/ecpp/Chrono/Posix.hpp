/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_CHRONO_POSIX_HPP_
#define ECPP_CHRONO_POSIX_HPP_

#include "ecpp/Chrono/Duration.hpp"
#include "ecpp/Chrono/Timepoint.hpp"
#include <ctime>

namespace ecpp::Chrono
{
  template<>
  struct Duration_Trait<::std::time_t>
  {
    static constexpr ::std::time_t kInvalid {0};
  };

  class PosixSystemClock
  {
  public:
    using Duration  = ::std::time_t;
    using Timepoint = Chrono::Timepoint<UtcClockTag, Duration>;

    static Timepoint now() { return { Duration(::std::time(NULL))}; }
    
    static void SetTime(const Timepoint tp);
  };

  class PosixBrokenDownTime : public tm
  {
  public:
    constexpr PosixBrokenDownTime () : tm{} {}

    template<class InitTrait>
    constexpr PosixBrokenDownTime (const InitTrait& dummy, PosixSystemClock::Timepoint init) : tm{} 
    { 
      dummy.init(this, init); 
    }

    constexpr PosixBrokenDownTime (const struct tm& init) : tm(init) {}
    constexpr PosixBrokenDownTime (const Date &date, const TimeOfDay &time) : tm(ConvertFrom(date, time)) {}

    constexpr TimeOfDay time() const { return {(uint_least8_t)tm_hour, (uint_least8_t)tm_min, (uint_least8_t)tm_sec}; }
    constexpr Date      date() const { return {(uint_least16_t)(1900 + tm_year), (uint_least8_t)(tm_mon + 1), (uint_least8_t)tm_mday}; }

    template<typename Clock> class InitTrait;

    static const PosixBrokenDownTime kInvalid;
  private:
    static constexpr struct tm ConvertFrom(const Date &date, const TimeOfDay &time)
    {
      return {
        .tm_sec  = time.seconds(),
        .tm_min  = time.minutes(),
        .tm_hour = time.hours(),
        .tm_mday = date.mday(),
        .tm_mon  = date.month() - 1,
        .tm_year = date.year() - 1900,
        .tm_wday  = 0,
        .tm_yday  = 0,
        .tm_isdst = -1, /* let system compute dst information by itself */
      };
    }
  };

  constexpr const PosixBrokenDownTime PosixBrokenDownTime::kInvalid {};

  template<>
  class PosixBrokenDownTime::InitTrait<LocalClockTag>
  {
  public:
    static void init( ::std::tm *target, PosixSystemClock::Timepoint init)
    {
      localtime_r(&init.time_since_epoch(), target);
    }
  };

  template<>
  class PosixBrokenDownTime::InitTrait<UtcClockTag>
  {
  public:
    static void init( ::std::tm *target, PosixSystemClock::Timepoint init)
    {
      gmtime_r(&init.time_since_epoch(), target);
    }
  };


  template<typename Clock_>
  class Timepoint<Clock_, PosixBrokenDownTime>
  {
  public:
    using Duration = PosixBrokenDownTime;
    using Clock    = Clock_;

    constexpr Timepoint() : duration_(Duration::kInvalid) {}
    constexpr Timepoint(const Timepoint &init) : duration_(init.duration_)  {}
    constexpr Timepoint(const PosixSystemClock::Timepoint init) : duration_(PosixBrokenDownTime::InitTrait<Clock>(), init) 
      {}

    operator PosixSystemClock::Timepoint() const   
    { 
      struct tm t = duration_;
      return { ::std::mktime(&t) }; 
    }

    void set(const TimeOfDay& t) 
    { 
      duration_.tm_hour = t.hours(); 
      duration_.tm_min  = t.minutes(); 
      duration_.tm_sec  = t.seconds(); 
    }

    constexpr auto time() const { return duration_.time(); }
    constexpr auto date() const { return duration_.date(); }
    
    WeekDay wday() const { return WeekDay(duration_.tm_wday); }

    constexpr bool valid() const { return duration_.tm_mday != 0;}

    static constexpr Timepoint from_duration(const Duration &init) { return {init}; }
  private:
    constexpr Timepoint(const Duration &init) : duration_(init)  {}

    Duration duration_;
  };

  template<class Clock>
  bool operator <= (const Timepoint<LocalClockTag, PosixBrokenDownTime> &lhs, const Timepoint<Clock, ::std::time_t> &rhs)
  {
    return static_cast<Timepoint<Clock, ::std::time_t> >(lhs) <= rhs;
  }

  class PosixLocalClock
  {
  public:
    using Duration  = PosixBrokenDownTime;
    using Timepoint = Chrono::Timepoint<LocalClockTag, Duration>;

    template<typename Timezone_>
    static void SetTimezone(const Timezone_ &tz ) { SetTimezone(tz.tz_env_ + 3); }

    static Timepoint now() { return PosixSystemClock::now(); }
  private:
    static void SetTimezone(const char* tz_var);
  };
}
#endif