/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_CHRONO_DURATION_HPP_
#define ECPP_CHRONO_DURATION_HPP_

#include <cstdint>

/** Provides Time, Date & Calendar handling in a microcontroller 
 *  friendly design.
 * 
 *  Inspired by std:chrono but with lots of simplifications (thus 
 *  some limitations) */
namespace ecpp::Chrono
{
  template<typename Clock, typename Duration>
  class Timepoint;

  class WeekDay
  {
  public:
    static const WeekDay Sunday;
    static const WeekDay Monday;
    static const WeekDay Tuesday;
    static const WeekDay Wednesday;
    static const WeekDay Thursday;
    static const WeekDay Friday;
    static const WeekDay Saturday;

    explicit constexpr WeekDay(uint_least8_t init) : wday_(init) {}

    constexpr operator unsigned int() const { return wday_; }
  private:

    const uint_least8_t wday_;

    template<typename Clock, typename Duration>
    friend class Timepoint;
  };

  constexpr const WeekDay WeekDay::Sunday{0};
  constexpr const WeekDay WeekDay::Monday{1};
  constexpr const WeekDay WeekDay::Tuesday{2};
  constexpr const WeekDay WeekDay::Wednesday{3};
  constexpr const WeekDay WeekDay::Thursday{4};
  constexpr const WeekDay WeekDay::Friday{5};
  constexpr const WeekDay WeekDay::Saturday{6};


  /** Represent the time of day */
  class TimeOfDay
  {
  public:
    constexpr TimeOfDay(uint_least8_t hours, uint_least8_t minutes, uint_least8_t seconds) :
       hours_(hours), minutes_(minutes), seconds_(seconds) {};

    constexpr uint_least8_t hours()   const { return hours_;}
    constexpr uint_least8_t minutes() const { return minutes_;}
    constexpr uint_least8_t seconds() const { return seconds_;}
  private:
    uint_least8_t hours_;
    uint_least8_t minutes_;
    uint_least8_t seconds_;
  };

  /** Represents a date */
  class Date
  {
  public:
    constexpr Date(uint_least16_t year, uint_least8_t month, uint_least8_t mday) :
      century_(year/100), year_of_century_(year%100), month_(month), mday_(mday) {}

    constexpr uint_least16_t year()  const {return century_  * 100 + year_of_century_;}
    constexpr uint_least16_t month() const {return month_;}
    constexpr uint_least16_t mday()  const {return mday_;}

  private:
    uint_least8_t century_;
    uint_least8_t year_of_century_;
    uint_least8_t month_;
    uint_least8_t mday_;
  };

  /** Represents amount of passed time & date */
  class DateTimePassed
  {
  public:
    constexpr DateTimePassed(const TimeOfDay &time, const Date & date) : time_(time), date_(date) {}
  private:
    const TimeOfDay time_;
    const Date      date_;
  };  
}

#endif