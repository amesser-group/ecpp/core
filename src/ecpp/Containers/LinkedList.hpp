/*
 *  Copyright 2023 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_CONTAINERS_LINKEDLIST_HPP_
#define ECPP_CONTAINERS_LINKEDLIST_HPP_

namespace ecpp::Containers
{
  template<typename T>
  class ListItem;

  template<typename T>
  class LinkedList;

  template<typename T>
  class ListAnchor
  {
  protected:
    ListItem<T>* next_ = nullptr;

    ListItem<T>* RemoveNext() 
    { 
      ListItem<T>* n = next_;

      if(n != nullptr)
        next_ = n->next_;

      return n;
    }

    friend class ListItem<T>;
  };

  template<typename T>
  class ListItem : public ListAnchor<T>
  {
  public:
    operator T*() {return static_cast<T*>(this);}

  protected:
    void InsertAfter(ListAnchor<T> *after) {ListAnchor<T>::next_ = after->next_; after->next_ = this; }
    friend class LinkedList<T>;
  };

  template<typename T>
  class LinkedList : private ListAnchor<T>
  {
  public:
    constexpr LinkedList() : back_(this) {}

    bool empty() const { return ListAnchor<T>::next_ != nullptr; }

    T*   front() const { return static_cast<T*>(ListAnchor<T>::next_); }

    void push_front(ListItem<T> *item);
    void push_back(ListItem<T> *item);

    T*   pop_front();

  protected:
    ListAnchor<T>* back_;
  };

  template<typename T>
  void LinkedList<T>::push_front(ListItem<T> *item)
  {
    item->InsertAfter(this);

    if(back_ == this)
      back_ = item;
  }

  template<typename T>
  void LinkedList<T>::push_back(ListItem<T> *item)
  {
    item->InsertAfter(back_);
    back_ = item;
  }

  template<typename T>
  T* LinkedList<T>::pop_front()
  {
    auto front = this->RemoveNext();

    if(front == back_)
      back_ = this;

    return static_cast<T*>(front);
  }




}
#endif