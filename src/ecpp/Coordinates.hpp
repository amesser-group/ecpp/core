/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_COORDINATES_HPP_
#define ECPP_COORDINATES_HPP_

namespace ecpp
{
  /** Representation of a point in space */
  template<typename Addr>
  class Point
  {
  public:
    Addr x;
    Addr y;

    template<typename AddrX, typename AddrY>
    constexpr Point(AddrX x, AddrY y) : x{static_cast<Addr>(x)}, y{static_cast<Addr>(y)} {}

    constexpr bool operator!= (const Point &rhs) const { return (x != rhs.x) || (y != rhs.y); }
  };

  template<typename _Point>
  class Rect
  {
  public:
    template<typename Left, typename Upper, typename Right, typename Lower>
    constexpr Rect(Left x1, Upper y1, Right x2, Lower y2) 
      : upper_left{x1,y1}, lower_right{x2,y2} {}

    _Point upper_left;
    _Point lower_right;

    constexpr auto GetWidth() const { return lower_right.x - upper_left.x; }
    constexpr auto GetHeight() const { return lower_right.y - upper_left.y; }
  };
}

#endif