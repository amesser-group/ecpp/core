/*
 *  Copyright 2019 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_UNITS_TIME_HPP_
#define ECPP_UNITS_TIME_HPP_
#include <type_traits>

#include "ecpp/Units/Common.hpp"
#include "ecpp/Datatypes.hpp"

namespace ecpp::Units
{
  template<unsigned long int _TPower>
  struct DecimalMultiplier
  {
    template<typename ValueType>
    static constexpr ValueType Calc(ValueType value, ValueType add = 0) {return DecimalMultiplier<_TPower - 1>::Calc(value) * 10; }
  };

  template<>
  struct DecimalMultiplier<0>
  {
    template<typename ValueType>
    static constexpr ValueType Calc(ValueType value, ValueType add = 0) {return value; }
  };

  template<unsigned long int _TPower>
  struct DecimalDivider
  {
    template<typename ValueType>
    static constexpr ValueType Calc(ValueType value, ValueType add = 0) {return (DecimalDivider<_TPower - 1>::Calc(value) + add) / 10; }
  };

  template<>
  struct DecimalDivider<0>
  {
    template<typename ValueType>
    static constexpr ValueType Calc(ValueType value, ValueType add = 0) {return value;}
  };


  template<long int _TPowerChange>
  using DecimalRescaler = typename std::conditional< (_TPowerChange < 0), DecimalDivider<-_TPowerChange>, DecimalMultiplier<_TPowerChange> >::type;


  template<int Power> 
  class DecimalScale
  {
  public:
    /** the decimal power of the scale */
    static constexpr auto kDecimalPower = Power;


    /** Rescaling should not cut precision. It will fail if precision is lost
     *  use Floor(), Ceil() or Round() to accept precision looses*/
    template<typename ValueType>
    static constexpr ValueType Rescale(ValueType value, const DecimalScale & sc) { return value; }

    template<typename ValueType, int Pw>
    static constexpr ValueType Rescale(ValueType value, const DecimalScale<Pw>& sc) { return DecimalMultiplier<Power-Pw>::Calc(value, static_cast<ValueType>(0)); }

    /* Round the given value from Power to Pw */
    template<typename ValueType>
    static constexpr ValueType Round(ValueType value, const DecimalScale&)  { return value; }

    /* Round the given value from Power to Pw */
    template<typename ValueType, int Pw>
    static constexpr ValueType Round(ValueType value, const DecimalScale<Pw>& sc)  { return DecimalRescaler<Power-Pw>::Calc(value, static_cast<ValueType>(5)); }


    template<typename ValueType>
    static constexpr ValueType Floor(ValueType value, const DecimalScale&)  { return value; }

    template<typename ValueType, int Pw>
    static constexpr ValueType Floor(ValueType value, const DecimalScale<Pw>& sc)  { return DecimalRescaler<Power-Pw>::Calc(value, static_cast<ValueType>(0)); }

    template<typename ValueType>
    static constexpr ValueType Ceil(ValueType value, const DecimalScale&)  { return value; }

    template<typename ValueType, int Pw>
    static constexpr ValueType Ceil(ValueType value, const DecimalScale<Pw>& sc)  { return DecimalRescaler<Power-Pw>::Calc(value, static_cast<ValueType>(9)); }
  };


  /** Time between two events */
  template<typename ValueType, typename Scale>
  class TimeSpan : protected Scale
  {
  public:
    using ScaleType = Scale;

    explicit constexpr TimeSpan()               : span_{0} {}
    explicit constexpr TimeSpan(ValueType span) : span_{span} {}

    constexpr bool operator <= (const TimeSpan &rhs) const { return span_ <= rhs.span_; }

    template<typename Vt, typename Sc>
    constexpr bool operator <= (const TimeSpan<Vt, Sc> &rhs) const { return CeilTo(Sc()) <= rhs.span_; }

    constexpr bool operator >= (const TimeSpan &rhs) const { return span_ >= rhs.span_; }

    template<typename Vt, typename Sc>
    constexpr bool operator >= (const TimeSpan<Vt, Sc> &rhs) const { return span_ >= rhs.CeilTo(ScaleType()); }

    constexpr operator bool () const { return span_ != 0; }

    /** Returns raw tick in requested scale rounded */
    template<typename Sc>
    constexpr ValueType RoundTo(const Sc& sc) const { return Scale::Round(span_, sc); }

    /** Returns raw tick in requested scale rounded upwards */
    template<typename Sc>
    constexpr ValueType CeilTo(const Sc& sc)  const { return Scale::Ceil(span_, sc); }

    /** Returns raw tick in requested scale rounded downwards */
    template<typename Sc>
    constexpr ValueType FloorTo(const Sc& sc) const { return Scale::Floor(span_, sc); }

  protected:
    ValueType span_;
  };

  template<typename ValueType, typename Scale>
  class FlexibleTimespan : public TimeSpan<ValueType, Scale>
  {
  public:
    using TimeSpan<ValueType, Scale>::TimeSpan;

    template<typename _Vt, typename _Sc>
    constexpr FlexibleTimespan(const TimeSpan<_Vt, _Sc> & init) : TimeSpan<ValueType, Scale>(init.CeilTo(Scale())) {}
  };


  template<typename T = unsigned, signed Power = 0, unsigned Base = 10>
  class FixedScaleTime : public FixedScaleQuantity<T, Power, Base>
  {
  private:
    using Unsigned = typename ::ecpp::TypeProperties<T>::UnsignedType;
    using Signed   = typename ::ecpp::TypeProperties<T>::SignedType;

  protected:
    explicit constexpr FixedScaleTime(const T & t) : FixedScaleQuantity<T, Power, Base>(t) {}

  public:
    constexpr FixedScaleTime() : FixedScaleQuantity<T, Power, Base>() {}

    constexpr FixedScaleTime(const FixedScaleTime<Unsigned, Power, Base> & init)   : FixedScaleQuantity<T, Power, Base>(init.Value) {}
    constexpr FixedScaleTime(const FixedScaleTime<Signed, Power, Base>   & init)   : FixedScaleQuantity<T, Power, Base>(init.Value) {}

    template<typename T_RHS>
    FixedScaleTime & operator= (const FixedScaleTime<T_RHS, Power, Base> & rhs)
    {
      FixedScaleQuantity<T, Power, Base>::operator=(rhs);
      return *this;
    }

    template<typename T_RHS>
    constexpr bool operator> (const FixedScaleTime<T_RHS, Power, Base> & rhs) const
    {
      return this->Value > rhs.Value;
    }

    template<typename T_RHS>
    constexpr auto operator/ (const FixedScaleTime<T_RHS, Power, Base> & rhs) const
    {
      return this->Value / rhs.Value;
    }
  };

  template<typename T = unsigned> using Second = TimeSpan<T, DecimalScale<0>>;

  template<typename T = unsigned> using MilliSecond = TimeSpan<T, DecimalScale<-3>>;

  template<typename T = unsigned> using MicroSecond = TimeSpan<T, DecimalScale<-6>>;
}

#endif