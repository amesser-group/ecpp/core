/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_ARRAYSTRING_HPP_
#define ECPP_ARRAYSTRING_HPP_

#include <array>
#include <cstdarg>
#include <cstdio>

#include "ecpp/String.hpp"
#include "ecpp/StringEncodings/Unicode.hpp"

namespace ecpp
{
  template<typename _Encoding, std::size_t BufferSize>
  class ArrayString : public StringSpan<_Encoding>, protected ::std::array<typename _Encoding::BufferElement, BufferSize>
  {
  protected:
    using array_type = ::std::array<typename _Encoding::BufferElement, BufferSize>;

  public:
    using typename StringSpan<_Encoding>::BufferElement;
    using typename StringSpan<_Encoding>::ConstIterator;
    using typename StringSpan<_Encoding>::Iterator;

    using Encoding = _Encoding;

    using array_type::data;
    using array_type::max_size;

    template<typename ...InitCont>
    constexpr ArrayString(typename _Encoding::BufferElement a, InitCont ...remain) :
      ::std::array<typename _Encoding::BufferElement, BufferSize>{a, remain...} {}

    using ::std::array<typename _Encoding::BufferElement, BufferSize>::array;
  
    ArrayString(const StringView<_Encoding> & view)
    {
      this->operator=(view);
    }

    template<typename RValueString>
    ArrayString(const RValueString & rhs)
    {
      this->operator=(rhs);
    }

    template<typename Src>
    void assignRawValue(const Src* src, std::size_t value_size)
    {
      memcpy(data(), reinterpret_cast<const typename _Encoding::BufferElement*>(src), std::min(value_size, BufferSize));
    }

    auto constexpr begin()        { return Iterator(data(), data() + max_size()); }
    auto constexpr begin()  const { return ConstIterator(data(), data() + max_size()); }
    auto constexpr cbegin() const { return ConstIterator(data(), data() + max_size()); }
    auto constexpr end()    const { return EndIterationTag(); }

    bool operator ==( const ArrayString &rhs) const
    {
      return asStdArray() == rhs.asStdArray();
    }

    bool operator !=( const ArrayString &rhs) const
    {
      return asStdArray() != rhs.asStdArray();
    }

    ArrayString & operator = (const ArrayString & rhs) = default;

    template<std::size_t OtherBufferSize>
    ArrayString & operator = (const ArrayString<_Encoding, OtherBufferSize>  & rhs)
    {
      strncpy(reinterpret_cast<char*>(data()), reinterpret_cast<const char*>(rhs.data()), max_size());
      return *this;
    }

    template<template<typename> typename RValueString>
    ArrayString & operator = (const RValueString<_Encoding> & rhs);

    template<typename RValueString>
    ArrayString & operator = (const RValueString & rhs);

    ArrayString & operator = (const char* rhs)
    {
      return operator=(StringPointer<StringEncodings::Utf8>(rhs));
    }

    ArrayString & operator = (const char val)
    {
      return operator=(StringView<StringEncodings::Utf8>(ConstIterator(&val, &val+1)));
    }

    constexpr operator StringView<_Encoding>() const
    {
      return StringView<_Encoding>(begin());
    }

    constexpr StringView<_Encoding> substring(std::size_t start, std::size_t length) const
    {
      return { ConstIterator(data() + min(start, max_size()), data() + min(start + length, max_size()))};
    }

    void IPrintF(const BufferElement* format, ...);

    auto Trim() const
    {
      return StringView<_Encoding>::Trim(*this);
    }

    constexpr auto CountCharacters() const { return ::std::distance(begin(), ConstIterator(nullptr, nullptr)); }

    constexpr array_type       & AsArray()       { return *this; }
    constexpr const array_type & AsArray() const { return *this; }
  protected:
    const array_type & asStdArray() const {return *this; }
  };

  template<typename _Encoding, std::size_t BufferSize>
  template<template<typename> typename RValueString>
  ArrayString<_Encoding, BufferSize> & ArrayString<_Encoding, BufferSize>::operator = (const RValueString<_Encoding> & rhs)
  {
    auto start = rhs.begin();
    auto end = start;
    size_t count;

    for(; end < rhs.end(); ++end);

    count = end.ToPointer() - start.ToPointer();

    if(count < BufferSize)
    {
      memcpy(static_cast<char*>(data()), static_cast<const char*>(start.ToPointer()), count*sizeof(BufferElement));
      *(Iterator(data() + count, data() + max_size())) = _Encoding::Codepoint::kStringEnd;
    }
    else
    {
      /* using memcopy here causes an invalid compiler waring/error with esp32 */
      for(size_t i = 0; i < BufferSize; ++i)
        array_type::operator[](i) = start.ToPointer()[i];
    }

    return *this;
  }

  template<typename _Encoding, std::size_t BufferSize>
  template<typename RValueString>
  ArrayString<_Encoding, BufferSize> & ArrayString<_Encoding, BufferSize>::operator = (const RValueString & rhs)
  {
    auto src = EncodingConverterIterator<_Encoding, RValueString>(rhs);
    auto dst = Iterator(data(), data() + max_size());

    memset(data(), 0x00, max_size());

    while((dst != end()) && (src != rhs.end()))
    {
      *dst = *src;
      ++dst;
      ++src;
    }

    while(dst != end())
    {
      *dst = _Encoding::Codepoint::kStringEnd;
      ++dst;
    }


    return *this;
  }

  template<typename _Encoding, std::size_t BufferSize>
  void ArrayString<_Encoding, BufferSize>::IPrintF(const BufferElement* format, ...)
  {
    ::std::va_list args;

    va_start(args, format);
    vsniprintf(data(), max_size(), format, args);
    va_end(args);      
  }
}

#endif