 /*
 *  Copyright 2016 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_MATH_OPERATIONS_HPP_
#define ECPP_MATH_OPERATIONS_HPP_ 
  
#include <cstdint>
#include <type_traits>

namespace ecpp
{
  /* Multiplication with maximum precision */
  template<typename LHS, typename RHS>
  static constexpr auto Multiply(LHS lhs, RHS rhs) 
  { 
    using IntegerType = ::ecpp::integer_by_bytes<sizeof(LHS) + sizeof(RHS)>;

    using ReturnType =  ::std::conditional< (::std::is_signed_v<LHS> or ::std::is_signed_v<RHS>), typename IntegerType::Signed, typename IntegerType::Unsigned >::type;

    return static_cast<ReturnType>(lhs) * rhs;
  }

  /* recursive template to determine optimum type to yield value */
  template<uint64_t divisor, typename DividendType>
  struct DivideTypeTrait{ using ResultType = DivideTypeTrait<divisor/2, DividendType>::ResultType;};

  /* abort condition */
  template<typename DividendType>
  struct DivideTypeTrait<1, DividendType> {};

  template<> struct DivideTypeTrait<1ULL,       int32_t> { using ResultType = int32_t;};
  template<> struct DivideTypeTrait<1ULL << 16, int32_t> { using ResultType = int16_t;};
  template<> struct DivideTypeTrait<1ULL,       int64_t> { using ResultType = int64_t;};
  template<> struct DivideTypeTrait<1ULL << 32, int64_t> { using ResultType = int32_t;};
  template<> struct DivideTypeTrait<2ULL,       uint64_t>{ using ResultType = uint64_t;};
  template<> struct DivideTypeTrait<1ULL << 32, uint64_t>{ using ResultType = uint32_t;};

  template<uint64_t divisor, typename DividendType>
  constexpr auto Divide(DividendType n) 
  {
    return static_cast<DivideTypeTrait<divisor,DividendType>::ResultType>(n / divisor);
  }

  template<typename DividendType, typename DivisorType>
  constexpr DividendType Divide(DividendType n, DivisorType d) 
  {
    /* enforce proper return type */
    using ReturnType =  ::std::conditional< (::std::is_signed_v<DividendType> or ::std::is_signed_v<DivisorType>),
      ::std::make_signed_t<DividendType>, ::std::make_unsigned_t<DividendType> >::type;

    return static_cast<ReturnType>(n) / d;
  }
}

#endif