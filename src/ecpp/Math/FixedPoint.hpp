/*
 *  Copyright 2016 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_MATH_FIXEDPOINT_HPP_
#define ECPP_MATH_FIXEDPOINT_HPP_

#include "ecpp/Datatypes.hpp"
#include "ecpp/Math/Fraction.hpp"
#include "ecpp/Math/Operations.hpp"

namespace ecpp::Math 
{
  template<typename TYPE, uint64_t SCALE>
  class FixedPoint;

#ifdef  _GLIBCXX_OSTREAM
  template<typename TYPE, uint64_t SCALE>
  std::ostream& operator<<(std::ostream& stream, const FixedPoint<TYPE,SCALE>& fp);
#endif

  template<typename TYPE, uint64_t SCALE>
  class FixedPoint
  {
  public:
    using ValueType = TYPE;
    using NormalizationType = typename UnsignedIntTypeEstimator<SCALE>::Type;

    struct RawValue { const ValueType value; };

    static constexpr NormalizationType kNormalization = SCALE;

    constexpr FixedPoint() {}

    constexpr FixedPoint(const FixedPoint &init) : value_ (init.value_) {}

    explicit constexpr FixedPoint(volatile FixedPoint &init) : value_ (init.value_) {}

    constexpr FixedPoint(double d) : value_ (static_cast<ValueType>(kNormalization*d)) {}

    template<typename _InitType, uint64_t _InitScale>
    constexpr FixedPoint(const FixedPoint<_InitType,_InitScale> & init) : value_(Divide<_InitScale>(Multiply(init.value_,kNormalization))) {};

    /* Todo: This implementation looses precision compared with
     * Multiply(init.num,kNormalization) /init.denom. However, when kNormalization == init.denom, 
     * Compiler of avr8 does not optimize this properly */
    template<typename Nominator, typename Denominator>
    constexpr FixedPoint(const Fraction<Nominator,Denominator> &init) : value_ (static_cast<ValueType>( Multiply(init.num,kNormalization/init.denom))) {}

    /* Todo: This implementation looses precision compared with
     * Multiply(init.num,kNormalization) /init.denom. However, when kNormalization == init.denom, 
     * Compiler of avr8 does not optimize this properly */
    template<typename Nominator, typename Denominator>
    constexpr FixedPoint(Nominator n,Denominator d) : value_ (static_cast<ValueType>(Divide(Multiply(n,kNormalization),d))) {}

    constexpr FixedPoint operator - () const
    {
      /* static cast is needed in case of short ints due to integer promotion */
      return FixedPoint(RawValue{static_cast<ValueType>(-value_)});
    }

    const FixedPoint operator -- (int)
    {
      auto current = *this;

      value_ -= kNormalization;

      return current;
    }

    constexpr FixedPoint operator - (const FixedPoint & rhs) const  { return RawValue{ static_cast<ValueType>(value_ - rhs.value_)};  }

    constexpr FixedPoint operator +  (const FixedPoint & rhs) const  { return RawValue{ static_cast<ValueType>(value_ + rhs.value_)};  }

    FixedPoint & operator += (const FixedPoint & rhs)   
    { 
      value_ = static_cast<ValueType>(value_ + rhs.value_);
      return *this;
    }

    template<typename _Rhs>
    constexpr FixedPoint operator * (_Rhs rhs) const  { return RawValue{value_ * rhs}; }

    template<typename _Rhs>
    constexpr FixedPoint operator / (_Rhs rhs) const  { return RawValue{value_ / rhs}; }

    constexpr bool operator !=  (const FixedPoint & rhs) const     {return value_ != rhs.value_;  }
    constexpr bool operator  >  (const FixedPoint & rhs) const     {return value_ >  rhs.value_;  }
    constexpr bool operator  >= (const FixedPoint & rhs) const     {return value_ >=  rhs.value_;  }
    constexpr bool operator  <  (const FixedPoint & rhs) const     {return value_ <  rhs.value_;  }
    constexpr bool operator  <= (const FixedPoint & rhs) const     {return value_ <= rhs.value_;  }

    constexpr RawValue raw() const {return RawValue{value_};}

    static constexpr FixedPoint FromRaw(ValueType raw) { return FixedPoint(RawValue{raw}); }


    template<typename FRIEND_TYPE, uint64_t FRIEND_SCALE>
    friend class FixedPoint;

#if 0
    template<typename FRIEND_TYPE>
    friend class Complex;
#endif

#ifdef  _GLIBCXX_OSTREAM
    friend std::ostream& operator<< <TYPE,uint64_t>(std::ostream& stream, const FixedPoint& fp);
#endif
    protected:
      

      constexpr FixedPoint(const RawValue &raw) : value_ (raw.value) {}

      ValueType value_ = 0;
  };


  template<typename T_LHS, uint64_t S_LHS, typename T_RHS, uint64_t S_RHS>
  constexpr FixedPoint<T_LHS, S_LHS> operator * (const FixedPoint<T_LHS, S_LHS> &lhs, const FixedPoint<T_RHS, S_RHS> &rhs)
  { 
    return FixedPoint<T_LHS, S_LHS>::FromRaw( { static_cast<T_LHS>(Divide<S_RHS>(Multiply(lhs.raw().value, rhs.raw().value)))} );  
  }

  template<typename T_LHS, uint64_t S_LHS, typename T_RHS, uint64_t S_RHS>
  constexpr FixedPoint<T_LHS, S_LHS> operator / (const FixedPoint<T_LHS, S_LHS> &lhs, const FixedPoint<T_RHS, S_RHS> &rhs)
  { 
    return FixedPoint<T_LHS, S_LHS>::FromRaw( { static_cast<T_LHS>(Divide(Multiply(lhs.raw().value, S_RHS), rhs.raw().value))});  
  }


  template<typename T, uint64_t S>
  constexpr bool operator > (const FixedPoint<T,S> &lhs, int rhs)
  {
    return lhs > FixedPoint<T,S>( MakeFraction(rhs, 1));
  }

#ifdef  _GLIBCXX_OSTREAM
  template<typename TYPE, uint64_t SCALE>
  std::ostream& operator<<(std::ostream& stream, const FixedPoint<TYPE,SCALE>& fp)
  {
    return (stream << static_cast<double>(fp.m_value) / SCALE);
  }
#endif
}

#endif /* ECPP_MATH_FIXEDPOINT_HPP_ */
