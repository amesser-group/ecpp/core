/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_STRINGENCODING_H_
#define ECPP_STRINGENCODING_H_

#include "ecpp/String.hpp"

namespace ecpp
{
  class StringEncoding 
  {
  public:
    class Codepoint {};
  };

  template<typename _BufferElement, typename _Codepoint>
  class OneToOneStringEncoding : public StringEncoding
  {
  public:
    using BufferElement = _BufferElement;
    using Codepoint     = _Codepoint;

    class Encoder;
  };

  template<typename _BufferElement, typename _Codepoint>
  class OneToOneStringEncoding<_BufferElement, _Codepoint>::Encoder
  {
  public:
    constexpr Encoder(BufferElement *buf) : buf_{buf} {}

    BufferElement*  NextChar   (const BufferElement* end)
    {
      if((end == nullptr) || (buf_ < end))
        return (++buf_);
      else
        return end;      
    }

    BufferElement*  EncodeChar (Codepoint cp, const BufferElement* end)
    {
      if((end == nullptr) || (buf_ < end))
      {
        *buf_ = cp.GetRaw();
        return (++buf_);
      }
      else
      {
        return end;      
      }
    }

    constexpr void operator = (Codepoint cp)
    {
      if(this->buf_ != nullptr)
        *buf_ = cp.GetRaw();
    }

  protected:
    BufferElement *buf_;
  };
};

#endif